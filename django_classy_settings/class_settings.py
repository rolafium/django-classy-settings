import os
import sys
from importlib import import_module


class DjangoClassSettings(object):

    BASE_DIR = ""
    SECRET_KEY = ""
    DEBUG = False

    ALLOWED_HOSTS = []
    INSTALLED_APPS = []
    MIDDLEWARE = []

    ROOT_URLCONF = ""

    TEMPLATES = []
    WSGI_APPLICATION = ""

    DATABASES = {}

    AUTH_PASSWORD_VALIDATORS = []

    LANGUAGE_CODE = ""

    USE_I18N = True
    USE_L10N = True
    USE_TZ = True
    TIME_ZONE = "UTC"

    STATIC_URL = '/static/'
    MEDIA_URL = '/media/'

    LOGGING = {}


    @classmethod
    def remove_settings(cls, input_object, output_object):
        """
        Copy the settings defined in input_object from output_object
        :param input_object: object
        :param output_object: object
        """
        remove_results = {
            "found": [],
            "not_found": [],
        }

        for element in dir(input_object):
            if not element.startswith("__"):
                setting_name = element
                try:
                    delattr(output_object, setting_name)
                    remove_results["found"].append(setting_name)
                except AttributeError:
                    remove_results["not_found"].append(setting_name)
        
        return remove_results


    @classmethod
    def copy_settings(cls, input_object, output_object):
        """
        Copy the settings defined in input_object to output_object
        :param input_object: object
        :param output_object: object
        """
        for element in dir(input_object):
            if not element.startswith("__"):
                setting_name = element
                setting_value = getattr(input_object, setting_name)
                setattr(output_object, setting_name, setting_value)

    @classmethod
    def export_vars(cls):
        """
        Exports all the current settings in the current module
        """
        from django.conf import global_settings
        if os.environ.get("DJANGO_SETTINGS_MODULE", None) == cls.__module__:
            current_module = import_module(cls.__module__)
            cls.copy_settings(global_settings, current_module)
            cls.copy_settings(cls, current_module)
