from django_classy_settings.class_settings import DjangoClassSettings


class MockSettingsClass(DjangoClassSettings):
    """Mock settings class"""

    MOCKED_NAME = "MOCK_SETTINGS_CLASS"
    MOCKED_VARIABLE = "MOCKED_VARIABLE"


class MockSettingsChildClass(MockSettingsClass):
    """Mock settings class child"""

    MOCKED_NAME = "MOCK_SETTINGS_CHILD_CLASS"
    NEW_MOCKED_VARIABLE = "NEW_MOCKED_VARIABLE"


class NotImportedSettings(DjangoClassSettings):
    SOME_VARIABLE_NOT_IMPORTED = "SOME_VARIABLE_NOT_IMPPORTED"
