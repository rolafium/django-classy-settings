import os
from unittest import TestCase
from django_classy_settings import class_settings
from django_classy_settings.tests import mocks


class TestClassSettings(TestCase):
    """Tests the DjangoClassSettings class"""

    def test_copy_settings(self):
        """Checks if all the attributes are copied from input to output classes"""
        
        class OutputClass(object):
            """Generic class"""
            pass

        mocks.MockSettingsClass.copy_settings(mocks.MockSettingsClass, OutputClass)

        self.assertEqual(getattr(OutputClass, "MOCKED_NAME"), mocks.MockSettingsClass.MOCKED_NAME)
        self.assertEqual(getattr(OutputClass, "MOCKED_VARIABLE"), mocks.MockSettingsClass.MOCKED_VARIABLE)


    def test_remove_settings(self):
        """Checks if all the attributes in input are removed in output"""

        class OutputClass(object):
            """Generic class"""
            MOCKED_NAME = "A"
            MOCKED_VARIABLE = "B"

        results = mocks.MockSettingsClass.remove_settings(mocks.MockSettingsClass, OutputClass)

        self.assertTrue("ALLOWED_HOSTS" in results["not_found"])
        self.assertTrue("DEBUG" in results["not_found"])

        self.assertTrue("MOCKED_NAME" in results["found"])
        self.assertTrue("MOCKED_VARIABLE" in results["found"])

        self.assertEqual(getattr(OutputClass, "MOCKED_NAME", None), None)
        self.assertEqual(getattr(OutputClass, "MOCKED_VARIABLE", None), None)


    def test_settings_exported_in_module(self):
        """Checks if the settings in the class are exported in the module"""
        os.environ["DJANGO_SETTINGS_MODULE"] = mocks.MockSettingsClass.__module__
        mocks.MockSettingsClass.remove_settings(mocks.MockSettingsClass, mocks)
        mocks.MockSettingsClass.export_vars()

        self.assertEqual(getattr(mocks, "MOCKED_NAME"), mocks.MockSettingsClass.MOCKED_NAME)
        self.assertEqual(getattr(mocks, "MOCKED_VARIABLE"), mocks.MockSettingsClass.MOCKED_VARIABLE)


    def test_settings_exported_in_module_inheritance(self):
        """Checks if the settings in the class are exported in the module"""
        os.environ["DJANGO_SETTINGS_MODULE"] = mocks.MockSettingsClass.__module__
        mocks.MockSettingsChildClass.remove_settings(mocks.MockSettingsChildClass, mocks)
        mocks.MockSettingsChildClass.export_vars()

        self.assertEqual(getattr(mocks,"MOCKED_NAME"), mocks.MockSettingsChildClass.MOCKED_NAME)
        self.assertEqual(getattr(mocks,"MOCKED_VARIABLE"), mocks.MockSettingsChildClass.MOCKED_VARIABLE)
        self.assertEqual(getattr(mocks,"NEW_MOCKED_VARIABLE"), mocks.MockSettingsChildClass.NEW_MOCKED_VARIABLE)


    def test_settings_exported_child_overrides_parent(self):
        """Checks if the settings in the child class override the parent class in the module"""
        os.environ["DJANGO_SETTINGS_MODULE"] = mocks.MockSettingsClass.__module__
        mocks.MockSettingsChildClass.remove_settings(mocks.MockSettingsChildClass, mocks)
        mocks.MockSettingsChildClass.export_vars()

        self.assertEqual(getattr(mocks,"MOCKED_NAME"), mocks.MockSettingsChildClass.MOCKED_NAME)
        self.assertEqual(getattr(mocks,"MOCKED_VARIABLE"), mocks.MockSettingsChildClass.MOCKED_VARIABLE)
        self.assertEqual(getattr(mocks,"NEW_MOCKED_VARIABLE"), mocks.MockSettingsChildClass.NEW_MOCKED_VARIABLE)


    def test_settings_not_exported_in_module(self):
        """Checks if the settings are not exported in the module when the DJANGO_SETTINGS_MODULE doesn't match"""
        os.environ["DJANGO_SETTINGS_MODULE"] = "ANOTHER_MODULE"
        mocks.NotImportedSettings.export_vars()

        self.assertEqual(getattr(mocks, "SOME_VARIABLE_NOT_IMPORTED", None), None)
