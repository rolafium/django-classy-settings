"""
test.py - Test runner
"""
import unittest


def run_tests(path):
    """
    Runs the tests located in the rovided path
    :param path: str
    """
    loader = unittest.TestLoader()
    suite = loader.discover(path)

    runner = unittest.TextTestRunner()
    return not runner.run(suite).wasSuccessful()


def init():
    """
    Wrapper over __name__ == "__main__" to allow testing
    """
    if __name__ == "__main__":
        return run_tests("./django_classy_settings/tests")

    return 1


exit(init())
