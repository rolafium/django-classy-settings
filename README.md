# Django Classy Settings

The Django Classy Settings library provides ready-to-use classes that allows the developers to defined class based settings in Django.

Setting modules contain a class which inherits from `DjangoClassSettings` and then call the class method `export_vars`. This will trigger an inheritance of settings (starting from the django global_settings) and it will place all the variables in the containing module (so no changes to manage.py and wsgi.py are required).

Settings class can be defined in a inheritance tree, so general settings can be overriden in a more specific settings file. This is the case when you define a `DevelopmentSettings` class from which each developer can inherit and define a gitignored personal settings file.


# Requirements

Tested with `Python 3.6` and Django `2.0`


# Installation

- Pip install the library: `pip install git+https://gitlab.com/rolafium/django-classy-settings.git`


# Usage

- Write a module with a class inheriting from `DjangoClassSettings` and calling `YourSettingsClass.export_vars()`:

```python
# your_module.settings.BaseSettings.py
from django_classy_settings import DjangoClassSettings


class BaseSettings(DjangoClassSettings):

    DEBUG = False
    SOME_SETTINGS = "SOME_SETTINGS"

# Settings are not exported, so this module cannot be used as DJANGO_SETTINGS_MODULE
```

```python
# your_module.settings.DevelopmentSettings.py
from your_module.settings.BaseSettings import BaseSettings


class DevelopmentSettings(BaseSettings):

    DEBUG = True
    SOME_OTHER_SETTINGS = "SOME_OTHER_SETTINGS"

DevelopmentSettings.export_vars()
# Note that `export_vars` checks if `DJANGO_SETTINGS_MODULE` matches `DevelopmentSettings.__module__`
# This means that if another module inherits from `DevelopmentSettings`
# this call to `export_vars` won't actually export the variables in this module
```

- Match the environment variable `DJANGO_SETTINGS_MODULE` with your settings module path, i.e.
```bash
# Settings for unit tests
DJANGO_SETTINGS_MODULE=your_module.settings.UnitTests python manage.py test
```
